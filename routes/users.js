var config = require('./config');
var validations = require('../helper/validation');
var helper = require('sendgrid').mail;
exports.userLogin = function (req, res) {
    var data = req.body;
    var email = data.email;
    var password = data.password;
    var type = data.type;


    if (!email) {
        res.json({"Status": false, "Message": "Email field is required "});
        return false;
    }
    else if (!password) {
        res.json({"Status": false, "Message": "Password field is required"});
        return false;
    }
    else if (!type) {
        res.json({"Status": false, "Message": "Type field is required"});
        return false;
    }
    else if (!validations.validEmail(email)) {
        res.json({"Status": false, "Message": "Please enter valid email"});
        return false;
    }
    else if (!validations.validPassword(password)) {
        res.json({"Status": false, "Message": "Password should not less than 6 letters"});
        return false;
    }
    else {
        req.getConnection(function (err, connection) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
                return false;
            }

            connection.query("SELECT * FROM tbl_user where user_email='" + email + "' and user_password='" + password + "' and user_type='" + type + "'", function (err, rows) {

                if (err) {
                    res.json({"Status": false, "Message": "Unable to get data.. ", "error": err});
                    return false;
                }
                if (rows.length > 0) {
                    if (type === 'admin') {
                        req.session['user_id'] = rows[0].user_id;
                        req.session['user_name'] = rows[0].user_name;
                        req.session['user_email'] = rows[0].user_email;
                        req.session['user_type'] = rows[0].user_type;


                    }
                    res.json({"Status": true, "Message": "User found", "data": rows[0]});
                    return false;
                }

                res.json({"Status": false, "Message": "Please enter valid email or password"});
                return false;
            });

            //console.log(query.sql);
        });
    }

};

exports.userFriend = function (req, res) {
    var data = req.body;
    var user_id = data.user_id;
    if (!user_id) {
        res.json({"Status": false, "Message": "User id is required "});
        return false;
    }
    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }

        var query = "select * from tbl_friends,tbl_user where tbl_friends.fr_user_id='" + user_id + "' and tbl_friends.fr_id = tbl_user.user_id";
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to get data.. ", "error": err});
                return false;
            }
            if (rows.length > 0) {
                res.json({"Status": true, "Message": "User found", "data": rows});
                return false;
            }

            res.json({"Status": false, "Message": "No records found"});
            return false;
        });

    });


};

exports.getAllUsers = function (req, res) {

    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }

        var query = "select * from tbl_user";
        connection.query(query, function (err, rows) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to get data.. ", "error": err});
                return false;
            }
            if (rows.length > 0) {
                res.json({"Status": true, "Message": "Users found", "data": rows});
                return false;
            }

            res.json({"Status": false, "Message": "No records found"});
            return false;
        });

    });
};

exports.userUpdate = function (req, res) {
    var data = req.body;
    var email = data.email;
    var password = data.password;
    var name = data.name;
    var id = data.id;

    if (!email) {
        res.json({"Status": false, "Message": "Email field is required "});
        return false;
    }
    else if (!password) {
        res.json({"Status": false, "Message": "Password field is required "});
        return false;
    }
    else if (!name) {
        res.json({"Status": false, "Message": "Name field is required "});
        return false;
    }
    else if (!id) {
        res.json({"Status": false, "Message": "Id is required "});
        return false;
    }
    else if (!validations.validEmail(email)) {
        res.json({"Status": false, "Message": "Email-id is not valid"});
        return false;
    }
    else if (!validations.validPassword(password)) {
        res.json({"Status": false, "Message": "Password is not valid"});
        return false;
    }

    var items = {
        user_name: name,
        user_email: email,
        user_password: password
    };

    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }
        var query = "UPDATE tbl_user set ? WHERE user_id = ? ";
        connection.query(query, [items, id], function (err, rows) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to update user data.. ", "error": err});
                return false;
            }
            res.json({"Status": true, "Message": "User data updated successfully"});
            return false;
        });

    });
};

exports.userInsert = function (req, res) {
    var data = req.body;
    var email = data.email;
    var password = data.password;
    var name = data.name;

    if (!email) {
        res.json({"Status": false, "Message": "Email field is required "});
        return false;
    }
    else if (!password) {
        res.json({"Status": false, "Message": "Password field is required "});
        return false;
    }
    else if (!name) {
        res.json({"Status": false, "Message": "Name field is required "});
        return false;
    }
    else if (!validations.validEmail(email)) {
        res.json({"Status": false, "Message": "Email-id is not valid"});
        return false;
    }
    else if (!validations.validPassword(password)) {
        res.json({"Status": false, "Message": "Password is not valid"});
        return false;
    }
    var items = {
        user_name: name,
        user_email: email,
        user_password: password,
        user_type:'user',
        user_status:'D'
    };

    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }

        connection.query("select * from tbl_user where user_email='" + email+ "'", function (err, rows) {

            if (err) {
                res.json({"Status": false, "Message": "Unable to get like data "});
                return false;
            }
            if (rows.length > 0) {
                res.json({"Status": false, "Message": "This email "+email+" already exists"});
                return false;

            }
            var query = "INSERT INTO tbl_user set ? ";
            connection.query(query, items, function (err, rows) {
                if (err) {
                    res.json({"Status": false, "Message": "Unable to insert user data.. ", "error": err});
                    return false;
                }

           var message ='<html><head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>' +
            '</head><body><div class="container"><div class="row"><div class="col-md-12"><h2 style="color: #8c231f;font-family: Times New Roman, Times, serif;font-weight: bold">THANK YOU FOR YOUR POST MASTER REGISTRATION</h2>' +
            '</div></div><div class="row"><div class="col-md-12"><p >Please click the follwing link to confirm that</p>' +
            '</div><div class="col-md-12"><p >'+email+' is your email address where you will </p></div><div class="col-md-12">\n' +
            '<p >receive reply to your issues</p></div><div class="col-md-12"><a href="http://codefish-appy.herokuapp.com/userSubscribe?id='+rows.insertId+'" class="btn btn-sm btn-primary ">Comfirm email address</button>\n' +
            '</div></div></div></body></html>';

            validations.sendMail(email,'info@postmaster.com','Post Master',message);
                res.json({"Status": true, "Message": "Check your email to activate account"});
                return false;
             });
        });
    });

};

exports.userOtpGenerate = function (req, res) {
    var data = req.body;
    var email = data.email;
    if (!email) {
        res.json({"Status": false, "Message": "Email field is not found"});
        return false;
    }
    else if (!validations.validEmail(email)) {
        res.json({"Status": false, "Message": "Please enter valid email"});
        return false;
    }
    else{
        req.getConnection(function (err, connection) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
                return false;
            }
            var query = "SELECT * FROM tbl_user where user_email='" + email + "' and user_status = 'A'";
            // console.log('query -- '+query);
            connection.query(query, function (err, rows) {

                if (err) {
                    res.json({"Status": false, "Message": "Unable to get data.. ", "error": err});
                    return false;
                }
                if (rows.length > 0) {

                    var otp = validations.randomString(5);
                    var message = '<p>Your one time password is : </p>'+otp;
                    var user_id = rows[0].user_id;

                    var query = "UPDATE tbl_user set ? WHERE user_id = ? ";
                    var items = {
                        user_otp:otp
                    };
                    connection.query(query, [items,user_id], function (err, rows) {
                        if (err) {
                            res.json({"Status": false, "Message": "Unable to update user data.. ", "error": err});
                            return false;
                        }
                        validations.sendMail(email,'nk3527@gmail.com','Post Master',message);
                        res.json({"Status": true, "Message": "Please check your otp in your Email-id"});
                        return false;
                    });
                }
                else{
                    res.json({"Status": false, "Message": "Please enter valid email or password"});
                    return false;

                }
            });

            //console.log(query.sql);
        });
    }

};

exports.userOtpVerify = function (req, res) {
    var data = req.body;
    // console.log(data);
    var email = data.email;
    var otp = data.otp;
    var password = data.password;

    if (!email) {
        res.json({"Status": false, "Message": "Email field is not found"});
        return false;
    }
    else if(!otp) {
        res.json({"Status": false, "Message": "Otp field is not found"});
        return false;
    }
    else if(!password) {
        res.json({"Status": false, "Message": "Password field is not found"});
        return false;
    }
    else if(!validations.validPassword(password)) {
        res.json({"Status": false, "Message": "Password field should not less than 6 letters"});
        return false;
    }
    else if (!validations.validEmail(email)) {
        res.json({"Status": false, "Message": "Please enter valid email"});
        return false;
    }
    else {
        var query = "SELECT * FROM tbl_user where user_email='" + email + "' and user_otp = '" + otp + "'";
        // console.log('query -- '+query);
        req.getConnection(function (err, connection) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
                return false;
            }
            connection.query(query, function (err, rows) {

                if (err) {
                    res.json({"Status": false, "Message": "Unable to get data.. ", "error": err});
                    return false;
                }
                if (rows.length > 0) {

                    var user_id = rows[0].user_id;
                    var query = "UPDATE tbl_user set ? WHERE user_id = ? ";
                    var items = {
                        user_password: password
                    };
                    connection.query(query, [items, user_id], function (err, rows) {
                        if (err) {
                            res.json({"Status": false, "Message": "Unable to update user data.. ", "error": err});
                            return false;
                        }
                        res.json({"Status": true, "Message": "User password updated successfully"});
                        return false;

                    });
                }
                else {
                    res.json({"Status": false, "Message": "Invalid otp is entered"});
                    return false;
                }
            });
        });
    }

};

exports.userStatusUpdate = function (req, res) {
    var data = req.body;
    var id = data.id;
    var status = data.status;

    if (!id) {
        res.json({"Status": false, "Message": "Id field is not found"});
        return false;
    }
    else if (!status) {
        res.json({"Status": false, "Message": "Status field is not found"});
        return false;
    }


    var items = {
      user_status:status
    };

    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }
        var query = "UPDATE tbl_user set ? WHERE user_id = ? ";
        connection.query(query, [items, id], function (err, rows) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to update user data.. ", "error": err});
                return false;
            }
            res.json({"Status": true, "Message": "User data updated successfully"});
            return false;
        });

    });
};

exports.deleteUser = function(req,res){

    var data = req.body;
    var id = data.id;
    if (!id) {
        res.json({"Status": false, "Message": "Id field is not found"});
        return false;
    }

    req.getConnection(function (err, connection) {

        connection.query("DELETE FROM tbl_user  WHERE user_id = ? ",[id], function(err, rows)
        {

            if(err)
            {
                res.json({"Status": false, "Message": "Unable to delete user .. ", "error": err});
                return false;
            }

            res.json({"Status": true, "Message": "User removed successfully .. "});
            return false;
        });

    });
};

exports.sendMail = function (req, res) {

  var message ='<html><head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>' +
        '</head><body><div class="container"><div class="row"><div class="col-md-12"><h2 style="color: #8c231f;font-family: Times New Roman, Times, serif;font-weight: bold">THANK YOU FOR YOUR POST MASTER REGISTRATION</h2>' +
        '</div></div><div class="row"><div class="col-md-12"><p >Please click the follwing link to confirm that</p>' +
        '</div><div class="col-md-12"><p >sksh3527@gmail.com is your email address where you will </p></div><div class="col-md-12">\n' +
        '<p >receive reply to your issues</p></div><div class="col-md-12"><button class="btn btn-sm btn-primary ">Comfirm email address</button>\n' +
        '</div></div></div></body></html>';

 validations.sendMail('sksh3527@gmail.com','nk3527@gmail.com','Post Master',message)
};
exports.adminLogin = function (req, res) {
    res.render('admin/login', {'title': 'Login'});
};

exports.subscribeUser = function (req, res) {
    res.render('admin/subscribe', {'title': 'Login','user_name':'sourav'});
};
