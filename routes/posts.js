var config = require('../routes/config');

exports.usersPost = function (req, res) {
    if(req.session['user_id']) {
        res.render('admin/posts',{title:'Posts','user_name':req.session['user_name']});
    }
    else{
        res.redirect('/login');
    }
};

exports.getPosts = function (req, res) {

    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }
        var query="SELECT * FROM tbl_posts where post_status='A' order by post_id desc";
        if(req.session['user_id']) {
            var user_type = req.session['user_id'];
            if(user_type === 'admin') {
                query = "select * from tbl_posts order by post_id desc";
            }
        }
        connection.query(query, function (err, rows) {

            if (err) {
                res.json({"Status": false, "Message": "Unable to get data.. ", "error": err});
                return false;
            }
            if (rows.length > 0) {

                res.json({"Status": true, "Message": "Post data found", "data": rows, "img_path": config.IMAGE_URL});
                return false;
            }

            res.json({"Status": false, "Message": "Please enter valid email or password"});
            return false;
        });

        //console.log(query.sql);
    });
};

exports.getComments = function (req, res) {

    var post_id = req.params.post_id;
    if (!post_id) {
        res.json({"Status": false, "Message": "user_id is required "});
        return false;
    }

    req.getConnection(function (err, connection) {

        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }

        connection.query("SELECT user_name,user_email,comm_text,comm_created_at FROM tbl_comments,tbl_user where tbl_comments.comm_user_id = tbl_user.user_id and comm_post_id='"+post_id+"'", function (err, rows) {

            if (err) {
                res.json({"Status": false, "Message": "Unable to get data.. ", "error": err});
                return false;
            }
            if (rows.length === 0) {

                res.json({"Status": false, "Message": "Comment data not found", "data": rows});
                return false;
            }
            res.json({"Status": true, "Message": "Comment data found", "data": rows});
            return false;

        });

        //console.log(query.sql);
    });
};

exports.updateLike = function (req, res) {
    var data = req.body;
    var user_id = data.user_id;
    var post_id = data.post_id;
    var like_count = data.like_count;

    if (!user_id) {
        res.json({"Status": false, "Message": "user_id is required "});
        return false;
    }
    if (!post_id) {
        res.json({"Status": false, "Message": "post_id is required "});
        return false;
    }
    if (!like_count) {
        res.json({"Status": false, "Message": "like_count is required "});
        return false;
    }

    var ins_items = {
        like_post_id: post_id,
        like_user_id: user_id
    };
    var up_items = {
        post_likes: like_count
    };

    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }
        connection.query("select * from tbl_likes where like_post_id='" + post_id + "' and like_user_id='" + user_id + "'", function (err, rows) {

            if (err) {
                res.json({"Status": false, "Message": "Unable to get like data "});
                return false;
            }
            if(rows.length >0) {
                res.json({"Status": false, "Message": "Like is already done"});
                return false;

            }
            connection.query("INSERT INTO tbl_likes set ? ", ins_items, function (err, rows) {
                if (err) {
                    res.json({"Status": false, "Message": "Unable to insert like"});
                    return false;
                }
                connection.query("UPDATE tbl_posts set ? WHERE post_id = ? ", [up_items, post_id], function (err, rows) {
                    if (err) {
                        res.json({"Status": false, "Message": "Unable to update post like"});
                        return false;
                    }

                    res.json({"Status": true, "Message": "Like updated successfully", "data": like_count});
                    return true;

                });
            });

        });
    });

};

exports.updateComments = function (req, res) {
    var data = req.body;
    var user_id = data.user_id;
    var post_id = data.post_id;
    var comm_count = data.comment_count;
    var comm_text = data.comment_text;

    if (!user_id) {
        res.json({"Status": false, "Message": "user_id is required "});
        return false;
    }
    if (!post_id) {
        res.json({"Status": false, "Message": "post_id is required "});
        return false;
    }
    if (!comm_count) {
        res.json({"Status": false, "Message": "comment_count is required "});
        return false;
    }
    if (!comm_text) {
        res.json({"Status": false, "Message": "comment text is required "});
        return false;
    }

    var ins_items = {
        comm_post_id: post_id,
        comm_user_id: user_id,
        comm_text: comm_text
    };
    var up_items = {
        post_comments: comm_count
    };

    req.getConnection(function (err, connection) {
        if (err) {
            res.json({"Status": false, "Message": "Unable to connect with db", "error": err});
            return false;
        }

        connection.query("INSERT INTO tbl_comments set ? ", ins_items, function (err, rows) {
            if (err) {
                res.json({"Status": false, "Message": "Unable to insert comment"});
                return false;
            }
            connection.query("UPDATE tbl_posts set ? WHERE post_id = ? ", [up_items, post_id], function (err, rows) {
                if (err) {
                    res.json({"Status": false, "Message": "Unable to update post comment"});
                    return false;
                }

                res.json({"Status": true, "Message": "Comment updated successfully", "data": comm_count});
                return true;

            });
        });

    });


};