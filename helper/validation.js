
var config = require("../routes/config");
var helper = require('sendgrid').mail;

var validations = {
    validEmail : function (email) {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!filter.test(email)) {

            return false;
        }
        return true;
    },
    validPassword: function (passwordText) {

        if(passwordText.length < 6) {
            return false;
        }
        return true;
    },
    sendMail : function(to,from,subject,message){
        var from_email = new helper.Email(from);
        var to_email = new helper.Email(to);

        var content = new helper.Content('text/html', message);
        var mail = new helper.Mail(from_email, subject, to_email, content);
        // var sg = require('sendgrid')("w2V6LjGdRL-SARDE46576w");
        var sg = require('sendgrid')(config.SENDGRID_API_KEY);
        var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });

        sg.API(request, function(error, response) {
            console.log(response.statusCode);
            console.log(response.body);
            console.log(response.headers);
        });
    },
    randomString : function(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }



};

module.exports = validations;