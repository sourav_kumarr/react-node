/**
 * Module dependencies.
 */

var bodyParser = require('body-parser');
var morgan = require('morgan');
var methodOverride = require('method-override');
// var handler = require('errorhandler');
var express = require('express');
var session = require('express-session');

var path = require('path');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var routes = require('./routes');
var config = require('./routes/config');

//load customers route
var customers = require('./routes/customers');
var index = require('./routes/index');
var users = require('./routes/users');
var posts = require('./routes/posts');

//load user route

//var users_register = require('./routes/users/register');

var connection  = require('express-myconnection'); 
var mysql = require('mysql');

// all environments
app.set('port', process.env.PORT || 4300);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//app.use(express.favicon());

//app.use(morgan.logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(methodOverride.me);

app.use(express.static(path.join(__dirname, 'public')));

app.use('/images',express.static(path.join(__dirname, 'public/images')));

app.use(express.cookieParser());
app.use(express.cookieSession({ secret: 'derp derp' }));

app.use(connection(mysql,{

        host: config.HOST,
        user: config.DB_USER,
        password : config.DB_PASSWORD,
        port : config.DB_PORT, //port mysql
        database:config.DB_NAME
    },'request')
);//route index, hello world


// development only
if ('development' === app.get('env')) {
  //app.use(handler.errorHandler());
}
//io = io.listen(http.createServer(app));

/*
* index page web service
*
* */

app.get('/',routes.index(app));

/*
* users web service
*
* */

app.post('/login',users.userLogin);
app.get('/login',users.adminLogin);
app.get('/users',users.getAllUsers);
app.post('/users/friends',users.userFriend);
app.post('/users/register',users.userInsert);
app.post('/users/otp',users.userOtpGenerate);
app.post('/users/otpVerify',users.userOtpVerify);
app.post('/users/update',users.userUpdate);
app.post('/users/updateStatus',users.userStatusUpdate);
app.post('/users/delete',users.deleteUser);
app.post('/users/mail',users.sendMail);
app.get('/userSubscribe',users.subscribeUser);


/*
 * posts web service
 *
 * */

app.get('/posts',posts.getPosts);
app.get('/userPosts',posts.usersPost);
app.get('/comments/(:post_id)',posts.getComments);
app.get('/postCreate',posts.postCreate);
app.post('/comment',posts.updateComments);
app.post('/like',posts.updateLike);

http.listen(app.get('port'),function(){
    console.log("Listening on 4300");
    // console.log(path.join(__dirname, 'public'));

});
exports.io = io;

