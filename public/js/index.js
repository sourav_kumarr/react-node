/**
 * Created by SachTech on 01-12-2017.
 */

var users = new Users();

function Users() {
    this.items = [];
    this.user_name = "";
    this.user_password = "";
    this.user_email = "";
    this.user_status = "";


    Users.prototype.validEmail = function () {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(this.user_email)) {
            return false;
        }
        return true;
    };
    Users.prototype.validPassword = function() {
        if(this.user_password.length < 6) {
            return false;
        }
        return true;
    };

    Users.prototype.validateUserData = function () {

        this.user_name = $("#user_name").val();
        this.user_password = $("#user_password").val();
        this.user_email = $("#user_email").val();
        this.user_status = $("#user_status").val();

        if(this.user_name === '' || this.user_password === '' || this.user_email === '' || this.user_status === '') {
            return false;
        }
        return true;
    }
};

window.onload = function() {
    getAllUsers();
};

function getAllUsers() {
    $(".loaderr").css("display","block");
    var url = "/users";
    $.get(url,function(data){
        var status = data.Status;
        var message = data.Message;
        var th_="<thead><tr><th>#</th><th>Name</th><th>Email</th><th>Password</th><th>Status</th><th>Created At</th><th>Operations</th>"+
            "</tr></thead><tbody>";
        $(".loaderr").css("display","none");
        if(status) {
            var items = data.data;
            var td_="";
            Users.items = items;
            for(var i=0;i<items.length;i++) {
                var user_status = items[i].user_status;
                var user_id = items[i].user_id;
                var user_date = new Date(items[i].user_created_on);
                console.log('user datee -- '+user_date.toDateString());
                var datee = user_date.getDate()+"-"+user_date.getMonth()+"-"+user_date.getFullYear()+" "+user_date.getHours()+" : "+user_date.getMinutes();
                var state_data = '<select id="user_status_'+user_id+'" onchange="updateUser(this,'+i+');"><option value="A">Active</option><option value="D">Disable</option></select>';
                if(user_status === 'D') {
                    state_data = '<select id="user_status_'+user_id+'" onchange="updateUser(this,'+i+');"><option value="A">Active</option><option value="D" selected="selected">Disable</option></select>';
                }
                td_ = td_+'<tr><td>'+(i+1)+'</td><td>'+items[i].user_name+'</td><td>'+items[i].user_email+'</td><td>'+items[i].user_password+'</td>'+
                    '<td>'+state_data+'</td><td>'+user_date.toDateString()+'</td><td>'+
                    '<button class="btn btn-danger" onclick="confirmDelete('+i+');"><i class="fa fa-trash-o"></i></button></td></tr>';
            }
            $("#userTable").html(th_+td_+"</tbody>");
        }
    });
}

/*function editUser(index) {
//        console.log(user_id);
    var name = Users.items[index].user_name;
    var email = Users.items[index].user_email;
    var password = Users.items[index].user_password;
    var status = Users.items[index].user_status;


    $(".modal-body").html("<div class='col-md-12 no-gutter'><div class='col-md-6 div-container'><label>Name</label><input type='text' id='user_name' placeholder='Enter user name' value='"+name+"' />" +
        "</div><div class='col-md-6 div-container'><label>Password</label><input type='text' id='user_password' placeholder='Enter user password' value='"+password+"'/></div>" +
        "<div class='col-md-6 div-container'><label>Email</label><input type='text' id='user_email' placeholder='Enter user email-id' value='"+email+"'/></div>" +
        "<div class='col-md-6 div-container'><label>Status</label><select id='user_status'><option value='A'>Active</option><option value='D'>Disable</option></select></div></div>");
    $(".modal-body").css("height","150px");
    $(".modal-footer").html('<label id="error" style="font-weight:bold;color:red;margin-right:20px"></label> <button type="button" class="btn btn-primary" id="close-it" onclick="updateUser('+index+')"><i class="fa fa-pencil"></i> Edit</button>' +
        '<button type="button" class="btn btn-danger" id="close-it" data-dismiss="modal"><i class="fa fa-trash-o"></i> Close</button>');
    $("#myModal1").modal("show");
}*/

function confirmDelete(index) {
    $(".modal-title").html("Delete");
    $(".modal-body").html('<label style="font-weight:bold">Would you like to delete user (y/n)?</label>');
    // $(".modal-body").css("height","100px");
    $(".modal-footer").html('<label id="error" style="font-weight:bold;color:red;margin-right:20px"></label> <button type="button" class="btn btn-default" id="close-it" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>'+
    '<button type="button" class="btn btn-danger" id="close-it" onclick="deleteUser('+index+');"><i class="fa fa-trash-o"></i> Delete</button>');
    $("#myModal1").modal("show");

}

function deleteUser(index) {
  var user_id = Users.items[index].user_id;
    var url = "/users/delete";

    $.post(url,{id:user_id},function(data) {
        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal1").modal("hide");
            setTimeout(function () {
                getAllUsers();
            },500);
        }
        else{
            $("#error").html(message);
        }
     });

}
function updateUser(ref,index) {

        var status = $("#"+ref.id).val();
        var user_id = Users.items[index].user_id;
        console.log('status --- '+status);

        var userPostData = {status:status,id:user_id};
        var url = "/users/updateStatus";
        $(".loaderr").css("display","block");
        $.post(url,userPostData,function(data) {
          var status = data.Status;
          var message = data.Message;
            $(".loaderr").css("display","none");
            $(".modal-title").html("Update");
            $(".modal-body").html('<label style="font-weight: bold">'+message+'</label>');
            // $(".modal-body").css("height","100px");
            $(".modal-footer").html('<button type="button" class="btn btn-default" id="close-it" data-dismiss="modal"><i class="fa fa-trash-o"></i> Close</button>');
            $("#myModal1").modal("show");
        });

  }