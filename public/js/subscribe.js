window.onload = function() {
  updateStatus();
};

function updateStatus() {
  var id = getQueryVariable("id");
  console.log('id --- '+id);
  var url = "users/updateStatus";
  $.post(url,{id:id,status:'A'},function(data){
     console.log('data--- '+data);
  });
}

function getQueryVariable(variable)
{
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
           var pair = vars[i].split("=");
           if(pair[0] == variable){return pair[1];}
   }
   return false;
}