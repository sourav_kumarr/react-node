/**
 * Created by
 * SachTech on
 * 11-12-2017.
 */
function Posts() {
  this.items = [];
}
var posts = new Posts();

window.onload = function () {
    getAllPosts();
};
function createPost() {
    window.location = "/postCreate";
}
function getAllPosts() {
    $(".loaderr").css("display","block");
    var url = "/posts";
    $.get(url,function (data) {
        var status = data.Status;
        var message = data.Message;
        $(".loaderr").css("display","none");
        var th_ = "<thead><tr><th>#</th><th>Image</th><th>Title</th><th>Likes</th><th>Comments</th><th>Status</th><th>Actions</th></tr></thead>";
        var td_ = "<tbody>";
        if(status ) {
          posts.items = data.data;
          for(var i=0;i<posts.items.length;i++) {
            var obj = posts.items[i];
            td_ = td_+'<tr><td>'+(i+1)+'</td><td><img class="thumbnail" src='+data.img_path+obj.post_image+' style="width:50px;height:50px"/></td><td>'+obj.post_title+'</td><td>'+obj.post_likes+'</td>' +
                '<td>'+obj.post_comments+'</td><td>'+obj.post_status+'</td><td><button class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>' +
                '&nbsp;&nbsp;<button class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></button></td></tr>';
          }
          $("#postTable").html(th_+td_+'</tbody>');
        }
        else{
           $(".table-responsive").html(message);
        }

    });
}